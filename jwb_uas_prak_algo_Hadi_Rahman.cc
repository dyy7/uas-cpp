#include <iostream>
using namespace std;


double hitungNilai(double absen, double tugas, double quiz, double uts, double uas);
char konversiHurufMutu(double nilai);


int main() {
    double absen = 100, tugas = 80, quiz = 40, uts = 60, uas = 50;
    char Huruf_Mutu;

    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;



    double nilai = hitungNilai(absen, tugas, quiz, uts, uas);

    
    Huruf_Mutu = konversiHurufMutu(nilai);

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}


double hitungNilai(double absen, double tugas, double quiz, double uts, double uas) {
    return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}


char konversiHurufMutu(double nilai) {
    char Huruf_Mutu;

    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B'; 
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';  
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';         
        Huruf_Mutu = 'D';  
     else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    return Huruf_Mutu;
}           


